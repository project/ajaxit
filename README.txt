This module utilizes the ajaxit plugin to convert a Drupal site into an ajax site. To use this module:

1. Download the ajaxit package from http://plugins.jquery.com/project/ajaxit
2. Go to the module folder and Place it inside a folder called ajaxit, the main pulgin files should be located in:
   /drupal-path-to-modules/ajaxit/ajaxit/ajaxit.js
   /drupal-path-to-modules/ajaxit/ajaxit/jquery.history.js
3. Enable the module
4. Go to settings and specify the jquery wrapper and other javascript callback functions (check documentation below)

For the plugin documentation, visit http://www.o-minds.com/products/ajaxit